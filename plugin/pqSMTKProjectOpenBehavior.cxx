//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqSMTKProjectOpenBehavior.h"

#include "pqSMTKProjectLoader.h"

// Client side
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqFileDialog.h"
#include "pqServer.h"

#include <QAction>
#include <QDebug>
#include <QDialog>
#include <QMessageBox>

//-----------------------------------------------------------------------------
pqProjectOpenReaction::pqProjectOpenReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqProjectOpenReaction::openProject()
{
  // Access the active server
  pqServer* server = pqActiveObjects::instance().activeServer();

  // Construct a file dialog for the user to select the project directory to load
  pqFileDialog fileDialog(server, pqCoreUtilities::mainWidget(), tr("Select Project (Directory):"));
  fileDialog.setObjectName("FileOpenDialog");
  fileDialog.setFileMode(pqFileDialog::Directory);
  fileDialog.setShowHidden(true);
  if (fileDialog.exec() != QDialog::Accepted)
  {
    return;
  }
  QString directory = fileDialog.getSelectedFiles()[0];
  pqSMTKProjectLoader::instance()->load(server, directory);
} // openProject()

//-----------------------------------------------------------------------------
static pqSMTKProjectOpenBehavior* g_instance = nullptr;

pqSMTKProjectOpenBehavior::pqSMTKProjectOpenBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqSMTKProjectOpenBehavior* pqSMTKProjectOpenBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqSMTKProjectOpenBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqSMTKProjectOpenBehavior::~pqSMTKProjectOpenBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}
