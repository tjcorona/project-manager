//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqSMTKProjectMenu_h
#define pqSMTKProjectMenu_h

//#include "Exports.h"

#include <QActionGroup>

class QAction;

class pqSMTKRecentProjectsMenu;

/** \brief Adds the "Project" menu to the application main window
  */
class pqSMTKProjectMenu : public QActionGroup
{
  Q_OBJECT
  using Superclass = QActionGroup;

public slots:
  void onProjectOpened();
  void onProjectClosed();

public:
  pqSMTKProjectMenu(QObject* parent = nullptr);
  ~pqSMTKProjectMenu() override;

  bool startup();
  void shutdown();

private:
  QAction* m_newProjectAction;
  QAction* m_openProjectAction;
  QAction* m_recentProjectsAction;
  QAction* m_saveProjectAction;
  QAction* m_closeProjectAction;
  QAction* m_exportProjectAction;

  pqSMTKRecentProjectsMenu* m_recentProjectsMenu;


  Q_DISABLE_COPY(pqSMTKProjectMenu);
};

#endif  // pqSMTKProjectMenu_h
