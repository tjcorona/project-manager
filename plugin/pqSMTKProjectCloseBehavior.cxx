//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqSMTKProjectCloseBehavior.h"

// SMTK
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKResource.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/io/Logger.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"

// // Client side
#include "pqActiveObjects.h"
#include "pqApplicationCore.h"
#include "pqCoreUtilities.h"
#include "pqObjectBuilder.h"
#include "pqServer.h"

#include <QAction>
#include <QDebug>
#include <QMessageBox>
#include <QtGlobal>

#include <vector>

//-----------------------------------------------------------------------------
pqProjectCloseReaction::pqProjectCloseReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqProjectCloseReaction::closeProject()
{
  // Access the active server and get the project manager
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto projectManager = wrapper->smtkProjectManager();

  // Create list of pqSMTKResource instances corresponding to the current project
  auto project = projectManager->getCurrentProject();
  std::string projectName = project->name();
  auto resourceList = project->resources();
  std::vector<pqSMTKResource*> pqResourceList;
  for (auto resource : resourceList)
  {
    auto pqResource = wrapper->getPVResource(resource);
    pqResourceList.push_back(pqResource);
  }

  // Delete the server-side resource instances
  auto& logger = smtk::io::Logger::instance();
  if (!projectManager->closeProject(logger))
  {
      std::string msg = logger.convertToString();
      if (msg.empty())
      {
        msg = "Internal error closing project";
      }
    QMessageBox::warning(pqCoreUtilities::mainWidget(), tr("Failed To Close Project"),
      tr(msg.c_str()));
    return;
  }

  // Remove the client-side (pqSMTKResource) instances
  pqApplicationCore* core = pqApplicationCore::instance();
  pqObjectBuilder* builder = core->getObjectBuilder();
  for (auto pqResource : pqResourceList)
  {
    builder->destroy(pqResource);
  }

  qInfo() << "Closed project" << projectName.c_str();
  emit this->projectClosed();
} // closeProject()

//-----------------------------------------------------------------------------
static pqSMTKProjectCloseBehavior* g_instance = nullptr;

pqSMTKProjectCloseBehavior::pqSMTKProjectCloseBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqSMTKProjectCloseBehavior* pqSMTKProjectCloseBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqSMTKProjectCloseBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqSMTKProjectCloseBehavior::~pqSMTKProjectCloseBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}
