//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqSMTKProjectExportBehavior.h"

// SMTK
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/ComponentItem.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/qt/qtOperationView.h"
#include "smtk/extension/qt/qtUIManager.h"
#include "smtk/io/Logger.h"
#include "smtk/model/Model.h"
#include "smtk/operation/Manager.h"
#include "smtk/project/Manager.h"
#include "smtk/project/Project.h"
#include "smtk/resource/Resource.h"

// Paraview client
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqServer.h"

// Qt
#include <QAction>
#include <QDebug>
#include <QDialog>
#include <QDir>
#include <QFileInfo>
#include <QMessageBox>
#include <QSharedPointer>
#include <QVBoxLayout>
#include <QtGlobal>

//-----------------------------------------------------------------------------
pqProjectExportReaction::pqProjectExportReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqProjectExportReaction::exportProject() const
{
  // Access the active server
  pqServer* server = pqActiveObjects::instance().activeServer();

  // Get project manager and export operator
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto projectManager = wrapper->smtkProjectManager();
  auto& logger = smtk::io::Logger::instance();
  bool reset = true;
  auto exportOp = projectManager->getExportOperator(logger, reset);
  if (exportOp == nullptr)
  {
    QMessageBox::warning(pqCoreUtilities::mainWidget(), tr("Failed Getting Export Operator"),
      tr(logger.convertToString().c_str()));
    return;
  }

  auto project = projectManager->getCurrentProject();
  this->populateOperator(exportOp, project);

  // Construct a modal dialog for the operation.
  QSharedPointer<QDialog> exportDialog = QSharedPointer<QDialog>(new QDialog());
  exportDialog->setObjectName("SimulationExportDialog");
  exportDialog->setWindowTitle("Simulation Export Dialog");
  exportDialog->setLayout(new QVBoxLayout(exportDialog.data()));
  // TODO: the dialog size should not be set this way. It should auto-expand
  // to accommodate the contained opView. Either Qt is being coy, or smtk's
  // qtBaseView logic for resizing doesn't inform the containing parent of its
  // decisions.
  exportDialog->resize(600, 300);

  // Create a new UI for the dialog.
  QSharedPointer<smtk::extension::qtUIManager> uiManager =
    QSharedPointer<smtk::extension::qtUIManager>(
      new smtk::extension::qtUIManager(exportOp, wrapper->smtkResourceManager()));

  // Create an operation view for the operation.
  smtk::view::ViewPtr view = uiManager->findOrCreateOperationView();
  smtk::extension::qtOperationView* opView = dynamic_cast<smtk::extension::qtOperationView*>(
    uiManager->setSMTKView(view, exportDialog.data()));

  exportDialog->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

  // Alert the user if the operation fails. Close the dialog if the operation
  // succeeds.
  smtk::operation::Operation::Result result;
  connect(opView, &smtk::extension::qtOperationView::operationExecuted,
    [=](const smtk::operation::Operation::Result& result) {
      if (result->findInt("outcome")->value() !=
        static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
      {
        QMessageBox msgBox;
        msgBox.setStandardButtons(QMessageBox::Ok);
        // Create a spacer so it doesn't look weird
        QSpacerItem* horizontalSpacer =
          new QSpacerItem(300, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);
        msgBox.setText("Export failed. Please see output log for more details.");
        QGridLayout* layout = (QGridLayout*)msgBox.layout();
        layout->addItem(horizontalSpacer, layout->rowCount(), 0, 1, layout->columnCount());
        msgBox.exec();

        // Once the user has accepted that their export failed, they are
        // free to try again without changing any options.
        opView->onModifiedParameters();
      }
      else
      {
        exportDialog->done(QDialog::Accepted);
      }
    });

  // Launch the modal dialog and wait for the operation to succeed.
  exportDialog->exec();
} // saveProject()

void pqProjectExportReaction::populateOperator(
  smtk::operation::OperationPtr op, smtk::project::ProjectPtr project) const
{
  // Simulation-specific code to set parameters in the export dialog
  // Ideally, this method will be deprecated in the future, as more workflow
  // functionality gets added to smtk.
  std::string simCode = project->simulationCode();
  if (simCode == "ace3p")
  {
    int paramVersion = op->parameters()->definition()->version();
    switch(paramVersion)
    {
      case 0:
      break;

      case 1:
      {
        smtk::attribute::ItemPtr item;
        QDir projectDir(QString::fromStdString(project->directory()));
        auto analysisItem = op->parameters()->findString("Analysis");
        // Output folder
        auto folderItem = analysisItem->findChild("OutputFolder", smtk::attribute::ALL_CHILDREN);
        if (folderItem)
        {
          QString exportPath = projectDir.path() + "/export";
          smtk::dynamic_pointer_cast<smtk::attribute::DirectoryItem>(folderItem)->setValue(0, exportPath.toStdString());
        }

        // Filename prefix
        item = analysisItem->findChild("OutputFilePrefix", smtk::attribute::ALL_CHILDREN);
        auto prefixItem = smtk::dynamic_pointer_cast<smtk::attribute::StringItem>(item);
        if (prefixItem)
        {
          prefixItem->setValue(project->name());
        }

        // Geometry file
        auto meshFileItem = op->parameters()->findFile("MeshFile");
        auto modelItem = op->parameters()->findComponent("model");
        if (meshFileItem && modelItem && modelItem->value(0))
        {
          auto modelResource = modelItem->value(0)->resource();
          std::string importLocation = project->importLocation(modelResource);
          if (!importLocation.empty())
          {
            QFileInfo meshFileInfo(QString::fromStdString(importLocation));

            // Check current project directory for same filename
            if (projectDir.exists(meshFileInfo.fileName()))
            {
              QString meshFilePath = projectDir.absoluteFilePath(meshFileInfo.fileName());
              meshFileItem->setValue(0, meshFilePath.toStdString());
            }
            else if (meshFileInfo.exists())
            {
              meshFileItem->setValue(0, meshFileInfo.absoluteFilePath().toStdString());
            }
            else
            {
              qWarning() << "Unable to find mesh file at" << meshFileInfo.absoluteFilePath();
            }
          }
        }  // if (meshFileItem && ...)


      }  // case 1
      break;

      default:
        qWarning() << "Unsupported parameters version" << paramVersion;
    }  // switch
  }  // if (ace3p)
  else if (simCode == "truchas")
  {
    int paramVersion = op->parameters()->definition()->version();
    switch(paramVersion)
    {
      case 0:
      {
        // Set project name
        auto projectNameItem = op->parameters()->findString("ProjectName");
        if (projectNameItem)
        {
          projectNameItem->setValue(project->name());
        }
      }  // case 0
      break;

      case 1:
      {
        QDir projectDir(QString::fromStdString(project->directory()));

        // Output file
        auto inpFileItem = op->parameters()->findFile("output-file");
        if (inpFileItem)
        {
          QString exportPath = projectDir.path() + "/export";
          QDir exportDir(exportPath);
          std::string inpFilename = project->name() + ".inp";
          QString inpFilePath = exportDir.absoluteFilePath(QString::fromStdString(inpFilename));
          inpFileItem->setValue(0, inpFilePath.toStdString());
        }

        // Mesh file
        auto meshFileItem = op->parameters()->findFile("mesh-file");
        auto modelItem = op->parameters()->findComponent("model");
        if (meshFileItem && modelItem && modelItem->value(0))
        {
          auto modelResource = modelItem->value(0)->resource();
          std::string importLocation = project->importLocation(modelResource);
          if (!importLocation.empty())
          {
            QFileInfo meshFileInfo(QString::fromStdString(importLocation));

            // Check current project directory for same filename
            if (projectDir.exists(meshFileInfo.fileName()))
            {
              QString meshFilePath = projectDir.absoluteFilePath(meshFileInfo.fileName());
              meshFileItem->setValue(0, meshFilePath.toStdString());
            }
            else if (meshFileInfo.exists())
            {
              meshFileItem->setValue(0, meshFileInfo.absoluteFilePath().toStdString());
            }
            else
            {
              qWarning() << "Unable to find mesh file at" << meshFileInfo.absoluteFilePath();
            }
          }
        }  // if (meshFileItem && ...)
      }  // case 1
      break;

      default:
        qWarning() << "Unsupported parameters version" << paramVersion;
    }  // switch
  }  // else if (truchas)
  // Todo other codes
}


//-----------------------------------------------------------------------------
static pqSMTKProjectExportBehavior* g_instance = nullptr;

pqSMTKProjectExportBehavior::pqSMTKProjectExportBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqSMTKProjectExportBehavior* pqSMTKProjectExportBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqSMTKProjectExportBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqSMTKProjectExportBehavior::~pqSMTKProjectExportBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}
