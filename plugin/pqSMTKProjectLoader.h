//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqSMTKProjectLoader_h
#define pqSMTKProjectLoader_h

#include <QObject>

class pqServer;
class pqServerResource;

/** \brief Handles opening projects from main menu and recent projects list
  */
class pqSMTKProjectLoader : public QObject
{
  Q_OBJECT
  typedef QObject Superclass;

public:
  static pqSMTKProjectLoader* instance(QObject* parent = nullptr);
  ~pqSMTKProjectLoader() override;


  // Indicates if paraview resource can be loaded
  bool canLoad(const pqServerResource& resource) const;

  // Open project from path on server
  bool load(pqServer *server, const QString& path);

  // Open project from recent projects list
  bool load(const pqServerResource& resource);

signals:
  void projectOpened();

protected:
  pqSMTKProjectLoader(QObject* parent = nullptr);

private:
  Q_DISABLE_COPY(pqSMTKProjectLoader);
};

#endif  // pqSMTKProjectLoader_h
