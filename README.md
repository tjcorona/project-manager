# SMTK Project Manager

Basic project management capabilities for smtk applications.
When loaded, the project plugin adds a top-level “Project” menu to modelbuilder, with 5 items:

  New Project…
  Save Project
  Close Project
  Load Project…
  Export Project…

The most interactive of these menu items is “New Project”. When selected:

1. The User is presented a UI panel to input a number of specifications:

* Which simulation to use (ACE3P, AdH, Albany, etc.)
* A new/empty directory on the local filesystem for storing project resources.
* A name for the project (maybe the project directory name?)
* The input model file. (Later versions of this plugin should support multiple models.)

2. In response, this plugin loads the specified model file and simulation template, and saves both resources to the project directory. TBD it also closes any other resources currently loaded, and TBD closes all views except the attribute editor, render view, and resource panel.

For the other project menu items, the plugin id designed to eliminate most of the manual steps that are currently needed. For example, the “Export Project” function would not require the user to locate the export script, nor assign the attribute and model items, nor create the output folder.

The initial implementation of this plugin supports only one project loaded at a time, mostly to keep things simple to the user.

# Building
This plugin requires Boost, ParaView, Qt5, SMTK and the smtkPQComponentsExt
plugin to build (as well as their dependencies). Due to CMB's use of
singleton behavior classes, you must link against the same SMTK
libraries as those used in the smtkPQComponentsExt plugin.

# License

CMB is distributed under the OSI-approved BSD 3-clause License.
See [License.txt][] for details.

[License.txt]: LICENSE.txt
